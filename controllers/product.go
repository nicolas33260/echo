package controllers

import (
	"echo/env"
	"echo/models"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"net/http"
)

func SaveProduct(c echo.Context) error {
	p := new(models.Product)

	if err := c.Bind(p); err != nil {
		return err
	}

	conn := c.Get("conn").(*env.Connexion)
	conn.DB.Create(p)

	return c.JSON(http.StatusCreated, p)
}

func GetProduct(c echo.Context) error {

	id := c.Param("id")
	conn := c.Get("conn").(*env.Connexion)

	var product models.Product
	if err := conn.DB.Find(&product, id).Error; gorm.IsRecordNotFoundError(err) {
		return c.JSON(http.StatusNotFound, map[string]string{"message": "Not found"})
	}

	return c.JSON(http.StatusOK, product)
}

func UpdateProduct(c echo.Context) error {
	p := new(models.Product)

	if err := c.Bind(p); err != nil {
		return err
	}

	id := c.Param("id")
	conn := c.Get("conn").(*env.Connexion)

	var product models.Product
	if err := conn.DB.Find(&product, id).Error; gorm.IsRecordNotFoundError(err) {
		return c.JSON(http.StatusNotFound, map[string]string{"message": "Not found"})
	}

	product.Code = p.Code
	product.Price = p.Price
	conn.DB.Save(&product)

	return c.JSON(http.StatusOK, product)
}

func DeleteProduct(c echo.Context) error {
	id := c.Param("id")
	conn := c.Get("conn").(*env.Connexion)

	var product models.Product
	if err := conn.DB.Find(&product, id).Error; gorm.IsRecordNotFoundError(err) {
		return c.JSON(http.StatusNotFound, map[string]string{"message": "Not found"})
	}
	conn.DB.Delete(&product)

	return c.JSON(http.StatusOK, product)
}