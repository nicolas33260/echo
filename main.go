package main

import (
	"echo/config"
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"os"
)

func main() {
	conn, err := config.Connect("root", "root", "localhost", "echo", true)
	handleErr(err)
	defer conn.DB.Close()

	e := echo.New()
	e.Debug = true

	e.Use(middleware.Logger())
	e.Use(conn.DatabaseMiddle)

	config.Router(e)
	e.Logger.Fatal(e.Start(":1323"))
}

func handleErr(err error)  {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
