package env

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

type Connexion struct {
	DB *gorm.DB
	User string
	Pass string
	Host string
	DbName string
}

func (conn *Connexion) DatabaseMiddle(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Set("conn", conn)
		return next(c)
	}
}