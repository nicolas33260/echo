package config

import (
	"echo/controllers"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func Router(e *echo.Echo) {
	//Auth
	e.POST("/auth/login", controllers.Login)

	products := e.Group("/products")
	products.Use(middleware.JWT([]byte("secret")))

	// Products
	products.POST("/", controllers.SaveProduct)
	products.GET("/:id", controllers.GetProduct)
	products.PUT("/:id", controllers.UpdateProduct)
	products.DELETE("/:id", controllers.DeleteProduct)
}
