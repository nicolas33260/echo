package config

import (
	"echo/env"
	"echo/models"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

const (
	driver = "mysql"
)

func Connect(user, pass, host, dbname string, migration bool) (*env.Connexion, error) {
	conn := &env.Connexion{
		User:   user,
		Pass:   pass,
		Host:  	host,
		DbName: dbname,
	}

	dsn := fmt.Sprintf("%s:%s@(%s:3306)/%s?charset=utf8&parseTime=True&loc=Local", user, pass, host, dbname)
	db, err := gorm.Open(driver, dsn)

	if err != nil {
		return conn, err
	}

	conn.DB = db

	if migration {
		RunMigration(conn)
	}

	return conn, nil
}

func RunMigration(conn *env.Connexion) {
	conn.DB.AutoMigrate(&models.Product{})
	conn.DB.AutoMigrate(&models.User{})
}
